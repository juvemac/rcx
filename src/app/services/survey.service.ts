import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Survey } from '../models/survey';
import { GLOBAL_API } from './global';

@Injectable()

export class SurveyService{
	public url:string;

	constructor(
		private _http:Http
	){
		this.url = GLOBAL_API.url;
	}

	getSurveys(page){
		let headers = new Headers({'Access-Control-Allow-Origin':'*',
								   'Access-Control-Allow-Methods': 'HEAD, GET, OPTIONS',
								});
		return this._http.get(this.url+'responses/'+page,{headers: headers}).map(res => res.json());
	}

	getSurvey(id){
		let headers = new Headers({'Access-Control-Allow-Origin':'*',
								   'Access-Control-Allow-Methods': 'HEAD, GET, OPTIONS',
								});
		return this._http.get(this.url+'responses/'+id,{headers: headers}).map(res => res.json());
	}

	getGraph(){
		let headers = new Headers({'Access-Control-Allow-Origin':'*',
								   'Access-Control-Allow-Methods': 'HEAD, GET, OPTIONS',
								});
		return this._http.get(this.url+'graph/',{headers: headers}).map(res => res.json());
	}
}