import { Sentiment } from '../models/sentiment';
import { Filter } from '../models/filter';

export var GLOBAL_API = {
	url: 'https://rcx.cisco.com/api/'
}

export var SENTIMENTS = [new Sentiment('5','Delighted','green accent-4 ',0,'sentiment_very_satisfied',0,false,false)
						,new Sentiment('4','Happy','lime accent-4 ',0,'sentiment_satisfied',0,false,false)
						,new Sentiment('3','Neutral','yellow accent-4 ',0,'sentiment_neutral',0,false,false)
						,new Sentiment('2','Frustrated','amber accent-4 ',0,'sentiment_dissatisfied',0,false,false)
						,new Sentiment('1','Furious','red accent-4 ',0,'sentiment_very_dissatisfied',0,false,false)];

export var FILTERS = 	[new Filter(1,'All',true),
						new Filter(2,'Last 24 hours',false),
						new Filter(3,'Last week',false),
						new Filter(4,'Last month',false),
						new Filter(5,'Last quarter',false)];

export var CHART_COLORS = [//delighted
							{backgroundColor: 'rgba(213, 0, 0, .7)',
						     borderColor: 'rgba(213, 0, 0, .9)',
						     pointBackgroundColor: 'rgba(213, 0, 0, .9)',
						     pointBorderColor: 'rgba(213, 0, 0, 1)',
						     pointHoverBackgroundColor: '#ffffff',
						     pointHoverBorderColor: 'rgba(213, 0, 0, 1)'
							},//happy
							{backgroundColor: 'rgba(255, 171, 0, .7)',
						     borderColor: 'rgba(255, 171, 0, .9)',
						     pointBackgroundColor: 'rgba(255, 171, 0, .9)',
						     pointBorderColor: 'rgba(255, 171, 0, 1)',
						     pointHoverBackgroundColor: '#ffffff',
						     pointHoverBorderColor: 'rgba(255, 171, 0, 1)'
							},//neutral
							{backgroundColor: 'rgba(255, 214, 0, .7)',
						     borderColor: 'rgba(255, 214, 0, .9)',
						     pointBackgroundColor: 'rgba(255, 214, 0, .9)',
						     pointBorderColor: 'rgba(255, 214, 0, 1)',
						     pointHoverBackgroundColor: '#ffffff',
						     pointHoverBorderColor: 'rgba(255, 214, 0, 1)'
							},
							//frustrated
							{backgroundColor: 'rgba(174, 234, 0, .7)',
						     borderColor: 'rgba(174, 234, 0, .9',
						     pointBackgroundColor: 'rgba(174, 234, 0, .9)',
						     pointBorderColor: 'rgba(174, 234, 0, 1)',
						     pointHoverBackgroundColor: '#ffffff',
						     pointHoverBorderColor: 'rgba(174, 234, 0, 1)'
							},//furious
							{backgroundColor: 'rgba(0, 200, 83, .7)',
						     borderColor: 'rgba(0, 200, 83, .9)',
						     pointBackgroundColor: 'rgba(0, 200, 83, .9)',
						     pointBorderColor: 'rgba(0, 200, 83, 1)',
						     pointHoverBackgroundColor: '#fffff',
						     pointHoverBorderColor: 'rgba(0, 200, 83, 1)'
							},];

export var CHART_OPTIONS = { scaleShowVerticalLines: false,
							    responsive: true,
							    tooltips:{
							    	mode: 'index',
							    	intersect: false,
							    },
							    hover:{
							    	mode: 'index'
							    },
							    scales: {
    							   	yAxes: [{
    							   		stacked: true,
			                            display: true,
			                            ticks: {
			                                beginAtZero: true,
			                                stepValue: 1,
			                            },
			                            gridLines: {
							                display:false
							            }
			                        }],
			                        xAxes:[{
			                        	gridLines: {
							                display:false
							            }
			                        }]
							    },
							    legend: {
							        display: true
							    },
							    maintainAspectRatio: true,
							    aspectRatio:3,
    						};

export var SURVEY_CHART_OPTIONS = { scaleShowVerticalLines: false,
    							    responsive: true,
    							    scales: {
	    							   	yAxes: [{
				                            display: true,
				                            ticks: {
				                                beginAtZero: true,
				                                stepValue: 1,
				                                max: 5,
				                                min: 0,
				                                fontFamily: 'Font Awesome\ 5 Free',
				                                callback: (value) => {
				                                	switch (value) {
				                                		case 0:
				                                			return "";
				                                		case 1:
				                                			return "😖";
														case 2:
				                                			return "😠";
				                                		case 3:
				                                			return "😔";
				                                		case 4:
				                                			return "😊";
				                                		case 5:
				                                			return "😀";
				                                	}
				                                }
				                            },
				                            gridLines: {
								                display:false
								            }
				                        }],
				                        xAxes:[{
				                        	gridLines: {
								                display:false
								            }
				                        }]
    							    },
    							    legend: {
								        display: false
								    },
    							  };