import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Survey } from '../models/survey';
import { Sentiment } from '../models/sentiment';
import { Filter } from '../models/filter';
import { SurveyService } from '../services/survey.service';
import { GLOBAL_API, SENTIMENTS, FILTERS, CHART_COLORS, SURVEY_CHART_OPTIONS, CHART_OPTIONS } from '../services/global';
declare var $:any;

import {
  trigger,
  state,
  style,
  animate,
  transition} from '@angular/animations';

@Component({
	selector: 'app-root',
	templateUrl: '../views/dashboard.view.html',
	animations: [
	    trigger('visibleInvisible', [
			state('visible', style({
				/*height: '600px',*/
				opacity: 1,
				overflow: 'hidden',
			})),
			state('invisible', style({
				height: '0px',
				opacity: 0,
				overflow: 'hidden',
			})),
			transition('visible => invisible', [
				animate('0.5s')
			]),
			transition('invisible => visible', [
				animate('0.5s')
			]),
		]),
  	],
  	styleUrls: ['../styles/dashboard.component.css']
})

export class DashboardComponent implements OnInit{
	public title:string;
	public surveys:Survey[];
	public survey:Survey;
	public url:string;
	public countSurveys:number;
	public sentiments:Sentiment[];
	public managers:string[];
	public workgroups:string[];
	public selectedManager:string;
	public selectedWorkgroup:string;
	public isVisible=true;

	public timeWindow=1;
	public filters:Filter[];
	public searchKey:string;

	public chartLabels:string[];
	public chartDatasets:any[];
	public chartOptions:{};
	public chartType='line';
	public chartColors:any[];

	public surveyChartLabels:string[];
	public surveyChartDatasets:any[];
	public surveyChartOptions:{};
	public surveyChartType='line';
	public surveyChartColors:any[];


	constructor(
		private _surveyService:SurveyService
	){
		this.title = "RCX | Surveys";
		this.url = GLOBAL_API.url;
		this.countSurveys = 0;
		this.survey = new Survey('',0,'','',0,'','','','','','','','','','',false);
		this.surveys = [];
		this.sentiments = SENTIMENTS;
		this.managers = ["All managers"];
		this.selectedManager = "All managers";
		this.workgroups = ["All workgroups"];
		this.selectedWorkgroup = "All workgroups";
		this.filters = FILTERS;

		this.chartLabels = [];
		this.chartDatasets = [];
		this.chartOptions = CHART_OPTIONS;
		this.chartColors = CHART_COLORS;

		this.surveyChartLabels = [];
		this.surveyChartDatasets = [];
		this.surveyChartOptions = SURVEY_CHART_OPTIONS;
    	this.surveyChartColors = [];
    	this.searchKey = '';
	}

	loadJqueryComponents(){
		$(document).ready(function(){
    		$('.dropdown-trigger').dropdown();
  		});
	}

	changeManager(manager){
		if (this.selectedManager == manager) {
			this.selectedManager = "All managers"
		}else{
			this.selectedManager = manager;
		}
		this.reloadData();
	}

	changeWorkgroup(workgroup){
		if (this.selectedWorkgroup == workgroup) {
			this.selectedWorkgroup = "All workgroups"
		}else{
			this.selectedWorkgroup = workgroup;
		}
		this.reloadData();
	}

	ngOnInit(){
		this.reloadData();
		this.getGraph();
		this.loadJqueryComponents();
	}


	reloadData(){
		this.sentiments.forEach((sentiment) => { sentiment._count = 0; sentiment.percentage = 0;});
		this.countSurveys = 0;
		this.surveys = [];
		this.getSurveys("");
	}

	getSurveys(page){

		this._surveyService.getSurveys(page).subscribe(
			result => {
				if (result.code && result.code != 200) {
					console.log("result.message_"+result.message);
				}else{
					this.countSurveys = result.count;
					this.processResponseLoadSurveys(result.results);

					if (result.next != null)
						this.getSurveys(result.next.substring(result.next.length-7,result.next.length))
				}
			}, error => {
				if (error.status == 403) {
					var jsonBody = JSON.parse(error._body);
					switch (jsonBody.detail) {
						case "SSO Cookie Missing":
						case "SSO verification service did not validate SSO cookie":
							window.location.href = "https://cloudsso.cisco.com/idp/startSSO.ping?PartnerSpId=https://techzone.cisco.com/auth/saml&redirectreason=notregistered&TARGET=https%3A%2F%2Frcx.cisco.com/dashboard/%2F";
							break;
						default:
							window.location.href = "https://rcx.cisco.com/unauthorized";
							break;
					}
				}
			}
		)
	}

	processResponseLoadSurveys(response:[]){		
		for (var i = 0; i < response.length; i++) {
			var currentSurvey = response[i];

			this.survey = new Survey(currentSurvey['url'],
									currentSurvey['id'],
									currentSurvey['timestamp'],
									currentSurvey['case'],
									currentSurvey['sentiment'],
									currentSurvey['comment'],
									currentSurvey['event_id'],
									currentSurvey['city'],
									currentSurvey['country_code'],
									currentSurvey['country_name'],
									currentSurvey['region'],
									currentSurvey['User agent'],
									currentSurvey['trend'],
									currentSurvey['manager'],
									currentSurvey['workgroup'],
									false);

			if(this.managers.indexOf(currentSurvey['manager']) == -1)
				if (currentSurvey['manager'] != "")
			  		this.managers.push(currentSurvey['manager']);

			if(this.workgroups.indexOf(currentSurvey['workgroup']) == -1)
				if (currentSurvey['workgroup'] != "")
			  		this.workgroups.push(currentSurvey['workgroup']);

			var sentiment = this.sentiments.find(x => x._id == String(this.survey.sentiment));
			sentiment._count += 1;
			sentiment.percentage = (sentiment._count*100/this.countSurveys);
			//sentiment.percentage = (sentiment._count*100/localCount);

			if (this.filterSurvey(currentSurvey))
				continue;

			if (this.managerFilterSurvey(currentSurvey))
				continue;

			if (this.workgroupFilterSurvey(currentSurvey))
				continue;

			if (this.timeFilterSurvey(currentSurvey))
				continue;

			this.surveys.push(this.survey);
		}
	}

	getGraph(){
		this._surveyService.getGraph().subscribe(
			result => {
				if (result.code && result.code != 200) {
					console.log(result.message);
				}else{
					this.processResponseLoadGraph(result.labels, result.datasets);
				}
			}, error => {
				if (error.status == 403) {
					var jsonBody = JSON.parse(error._body);
					switch (jsonBody.detail) {
						case "SSO Cookie Missing":
						case "SSO verification service did not validate SSO cookie":
							window.location.href = "https://cloudsso.cisco.com/idp/startSSO.ping?PartnerSpId=https://techzone.cisco.com/auth/saml&redirectreason=notregistered&TARGET=https%3A%2F%2Frcx.cisco.com/dashboard/%2F";
							break;
						default:
							window.location.href = "https://rcx.cisco.com/unauthorized";
							break;
					}
				}
			}
		)
	}

	processResponseLoadGraph(labels:[], datasets:any[]){
		this.chartLabels = labels;
		datasets.forEach((item) => {
			this.chartDatasets.push(item);
		});
	}

	filterSurvey(survey:Survey){
		var activeDelighted = this.sentiments[0].activeClass;
		var activeHappy = this.sentiments[1].activeClass;
		var activeNeutral = this.sentiments[2].activeClass;
		var activeFrustrated = this.sentiments[3].activeClass;
		var activeFurious = this.sentiments[4].activeClass;

		var sentiment = survey['sentiment'];

		var delighted = 5;
		var happy = 4;
		var neutral = 3;
		var frustrated = 2;
		var furious = 1;

		if (this.searchKey.trim().length > 0)
			if (!survey['case'].includes(this.searchKey))
				return true;

		if (!activeDelighted && !activeHappy && !activeNeutral && !activeFrustrated && !activeFurious)
			return false;
		if (sentiment == delighted)
			if (activeDelighted)
				return false;
		if (sentiment == happy)
			if (activeHappy)
				return false;
		if (sentiment == neutral)
			if (activeNeutral)
				return false;
		if (sentiment == frustrated)
			if (activeFrustrated)
				return false;
		if (sentiment == furious)
			if (activeFurious)
				return false;

		
		return true;
	}

	managerFilterSurvey(survey:Survey){
		if (this.selectedManager != "All managers") {
			if (survey.manager != this.selectedManager) {
				return true;
			}
		}
		return false;
	}

	workgroupFilterSurvey(survey:Survey){
		if (this.selectedWorkgroup != "All workgroups") {
			if (survey.workgroup != this.selectedWorkgroup) {
				return true;
			}
		}
		return false;
	}

	timeFilterSurvey(survey:Survey){
		var all = 1;
		var lastDay = 2;
		var lastWeek = 3;
		var lastMonth = 4;
		var lastQuarter = 5;

		if(this.timeWindow == all)
			return false;
		if(this.timeWindow == lastDay) 
			return !this.dateInRange(survey.timestamp, 1);
		if(this.timeWindow == lastWeek) 
			return !this.dateInRange(survey.timestamp, 7);
		if(this.timeWindow == lastMonth) 
			return !this.dateInRange(survey.timestamp, 30);
		if(this.timeWindow == lastQuarter) 
			return !this.dateInRange(survey.timestamp, 90);
		
		return false;
	}

	dateInRange(date:String, daysAgo:number) {
    	var checkingDate = new Date(date.toString());
	  	var today = new Date();

    	var timeAgo = new Date();
    	timeAgo.setDate(timeAgo.getDate() - daysAgo);

    	return (checkingDate > timeAgo && checkingDate <= today);
	}

	toogleChartVisibility(){
		this.isVisible = !this.isVisible;
	}

	toogleSentimentClass(sentiment:Sentiment){
		sentiment.activeClass = !sentiment.activeClass;
		if (this.sentiments.filter(x => x.activeClass).length == 5){
			this.sentiments.forEach((s) => {
				s.activeClass = false;
			});
		}
		this.reloadData();
	}

	onScroll(){
	}

	toogleSurveyChart(survey:Survey){
		this.surveyChartLabels = [];
		this.surveyChartDatasets = [];
		this.getSurvey(survey._id);

		if (survey.showChart) {
			survey.showChart = false;
		}else{
			this.surveys.forEach((s) => {
				s.showChart = false;
			});
			survey.showChart = true;
		}
	}

	getSurvey(id){
		this._surveyService.getSurvey(id).subscribe(
			result => {
				if (result.code && result.code != 200) {
					console.log(result.message);
				}else{
					this.processResponseSurvey(result);
				}
			}, error => {
				if (error.status == 403) {
					var jsonBody = JSON.parse(error._body);
					switch (jsonBody.detail) {
						case "SSO Cookie Missing":
						case "SSO verification service did not validate SSO cookie":
							window.location.href = "https://cloudsso.cisco.com/idp/startSSO.ping?PartnerSpId=https://techzone.cisco.com/auth/saml&redirectreason=notregistered&TARGET=https%3A%2F%2Frcx.cisco.com/dashboard/%2F";
							break;
						default:
							window.location.href = "https://rcx.cisco.com/unauthorized";
							break;
					}
				}
			}
		)
	}

	processResponseSurvey(response:[]){
		this.surveyChartLabels.push(response['timestamp'].substring(0,10));
		this.surveyChartDatasets.push(response['sentiment']);

		var priorResponses = response['prior_responses'];

		priorResponses.forEach((p_response) => {
			this.surveyChartLabels.push(p_response['timestamp'].substring(0,10));
			this.surveyChartDatasets.push(p_response['sentiment']);
		});

		this.surveyChartLabels.reverse();
		this.surveyChartDatasets.reverse();

		this.surveyChartDatasets = [{label: "Sentiment", data: this.surveyChartDatasets}];
		var sentiment = parseInt(response['sentiment']);
		this.surveyChartColors = [this.chartColors[sentiment - 1]];
	}

	onDeleteSearchKey(){
		if (this.searchKey == ''){
			this.reloadData();
		}
	}

	openCase(caseNumber:string){
		var url = 'https://scripts.cisco.com/app/quicker_csone/?sr='+caseNumber;
		var win = window.open(url, '_blank');
  		win.focus();
	}

	changeTimeWindow(filter:Filter){
		this.filters.forEach((f) => {
			f.active = false;
		});
		filter.active = true;
		this.timeWindow = filter.key;
		this.reloadData();
	}

	hoverGraph(sentiment:Sentiment){
		sentiment.activeTooltip = true;
	}

	leaveGraph(sentiment:Sentiment){
		sentiment.activeTooltip = true;
	}
}
