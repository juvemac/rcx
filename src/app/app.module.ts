import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './components/dashboard.component';
import { NavigationComponent } from './navigation-component/navigation.component';


import { SurveyService } from './services/survey.service';
import {TooltipModule} from 'ng2-tooltip-directive';

import { ChartsModule } from 'ng2-charts';
import {TimeAgoPipe} from 'time-ago-pipe';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';


@NgModule({
    declarations: [
      DashboardComponent,
      NavigationComponent,
      TimeAgoPipe,
    ],
    imports: [
      BrowserModule,
      AppRoutingModule,
      HttpModule,
      ChartsModule,
      BrowserAnimationsModule,
      InfiniteScrollModule,
      FormsModule,
      TooltipModule
    ],
    providers: [
      SurveyService
    ],
bootstrap: [DashboardComponent]
})
export class AppModule { }
