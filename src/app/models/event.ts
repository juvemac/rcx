export class Event{
	constructor(
		public city:String,
		public countryCode:String,
		public countryName:String,
		public region:String,
		public userAgent:String
	){}
}