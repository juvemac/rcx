export class Filter{
	constructor(
		public key:number,
		public name:String,
		public active:boolean,
	){}
}