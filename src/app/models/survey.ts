export class Survey{
	constructor(
		public url:String,
		public _id:Number,
		public timestamp:String,
		public caseNumber:String,
		public sentiment:Number,
		public comment:String,
		public event_id:String,
		public city:String,
		public country_code:String,
		public country_name:String,
		public region:String,
		public user_agent:String,
		public trend:String,
		public manager:String,
		public workgroup:String,
		public showChart:boolean
	){}
}