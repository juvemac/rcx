export class Sentiment{
	constructor(
		public _id:String,
		public name:String,
		public color:String,
		public _count:number,
		public icon:String,
		public percentage:number,
		public activeClass:boolean,
		public activeTooltip:boolean,
	){}
}